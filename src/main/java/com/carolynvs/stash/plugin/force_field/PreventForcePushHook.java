package com.carolynvs.stash.plugin.force_field;

import com.atlassian.stash.commit.CommitService;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.content.ChangesetsBetweenRequest;
import com.atlassian.stash.hook.HookResponse;
import com.atlassian.stash.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.RefChangeType;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageUtils;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PreventForcePushHook implements PreReceiveRepositoryHook
{
    private final CommitService commitService;
    private final I18nService i18nService;
    private final PathMatcher pathMater = new AntPathMatcher();

    public PreventForcePushHook(CommitService commitService, I18nService i18nService)
    {
        this.commitService = commitService;
        this.i18nService = i18nService;
    }

    @Override
    public boolean onReceive(@Nonnull RepositoryHookContext context, @Nonnull Collection<RefChange> refChanges, @Nonnull HookResponse response)
    {
        List<String> protectedRefs = getProtectedRefs(context);
        Repository repository = context.getRepository();

        List<String> blockedRefs = identifyReferencesWithBlockedChangesets(refChanges, protectedRefs, repository);

        if (!blockedRefs.isEmpty())
        {
            printError(response, blockedRefs);
        }

        return blockedRefs.isEmpty();
    }

    private List<String> getProtectedRefs(@Nonnull RepositoryHookContext context)
    {
        ArrayList<String> protectedRefs = new ArrayList<String>();

        String[] refs = context.getSettings().getString("references").split(" ");
        for(String ref : refs)
        {
            ref = ReferenceCleaner.fixUnmatchableRegex(ref);
            protectedRefs.add(ref);
        }
        return protectedRefs;
    }

    private List<String> identifyReferencesWithBlockedChangesets(Collection<RefChange> refChanges, List<String> protectedRefs, Repository repository)
    {
        List<String> blocked = new ArrayList<String>();
        for(RefChange refChange : refChanges)
        {
            String refId = refChange.getRefId();
            if(isProtectedRef(protectedRefs, refChange) && isForcePush(repository, refChange))
            {
                blocked.add(refId);
            }
        }
        return blocked;
    }

    private void printError(HookResponse response, List<String> blockedRefs)
    {
        response.out().println("=================================");
        response.out().println(i18nService.getText("force-field.plugin-name", "Force Field"));
        for (String refId : blockedRefs)
        {
            response.out().println(i18nService.getText("force-field.error-message", "The repository administrator has disabled force pushes to {0}", refId));
        }
        response.out().println("=================================");
    }

    private boolean isProtectedRef(List<String> restrictedRefs, RefChange refChange)
    {
        String refId = refChange.getRefId();
        for(String refPattern : restrictedRefs)
        {
            if(pathMater.match(refPattern, refId))
            {
                return true;
            }
        }

        return false;
    }

    private boolean isForcePush(Repository repository, RefChange refChange)
    {
        boolean isReplacingChangesets = refChange.getType() == RefChangeType.UPDATE;
        if(!isReplacingChangesets)
            return false;

        Page<Changeset> changes = getChangesets(repository, refChange);
        return changes.getSize() > 0;
    }

    private Page<Changeset> getChangesets(Repository repository, RefChange refChange)
    {
        ChangesetsBetweenRequest request = new ChangesetsBetweenRequest.Builder(repository)
                .exclude(refChange.getToHash())
                .include(refChange.getFromHash())
                .build();

        return commitService.getChangesetsBetween(request, PageUtils.newRequest(0, 1));
    }
}
